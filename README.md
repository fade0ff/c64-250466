Optimized version of the C64 Assy 250466 board with 12V supply, EEPROMs, DualSID, support for multiple kernals, joystick switch etc.

See http://www.lemmini.de/250466+/250466%20Plus.html

---------------------------------------------------------------
Copyright 2020 Volker Oth - VolkerOth(at)gmx.de

Licensed under the Creative Commons Attribution 4.0 license
http://creativecommons.org/licenses/by/4.0/

Everything in this repository is distributed on an "AS IS" BASIS, WITHOUT
WARRANTIES OF ANY KIND, either express or implied.